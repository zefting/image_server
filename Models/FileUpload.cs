﻿using System;
using System.Collections.Generic;

namespace Image_Server.Models
{
    public partial class FileUpload
    {
        public int Id { get; set; }
        public string OldFileName { get; set; }
        public string NewFileName { get; set; }
    }
}
