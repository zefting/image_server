
namespace Image_Server.Dtos
{
    public class DynamicJsonDto
    {
          public string NewFileName { get; set; }
          public int width  { get; set; }
          public int height {get; set;}
          
    }
}