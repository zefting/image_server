using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Image_Server.Data;
using Image_Server.Models;
using Image_Server.Dtos;
namespace Image_Server.Controllers
{
    [Route("api")]
    [ApiController]
    public class GetFileController : ControllerBase
    {
        
        private readonly IFileUploadRepo _repository;
        
        public GetFileController(IFileUploadRepo repository)
        {
            _repository = repository;
        }
        [HttpPost]
        [Route("getFile")]
        public FileStreamResult GetFileByName(FileUploadJsonDto getFileName)
        {
            // Get the upload basefolder. If this example no user is given, but here a user could be specificed a that users folder found. 
            string folderName = Path.Combine("StaticFiles", "Uploads");
            // Fetch the new filename from the database and populate it to the FileUpload model.
            var fileItem = _repository.GetFileByName(getFileName.NewFileName);
            // get the selected file's type and remove the dot.
            var fileType = Path.GetExtension(getFileName.NewFileName).Replace(".", "");
            // search the directory (and sub folders - SearchOption.AllDirectories) specified in folderName.
            string[] filePath = Directory.GetFiles(folderName,getFileName.NewFileName, SearchOption.AllDirectories);
            // Since each filename is uniq, the first item in the filePath array will always be the selected file. 
            string newFileNameFolderpath = filePath[0];
            // open a filesteam 
            var stream = new FileStream(newFileNameFolderpath, System.IO.FileMode.Open);
            // return the file to the user and set contenttype to image/ + fileType
            return new FileStreamResult(stream, "image/"+ fileType)
            {
                // rename the file to the users original filename 
                FileDownloadName = fileItem.OldFileName
            };
        }
    }
}