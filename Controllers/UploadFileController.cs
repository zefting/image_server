using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Image_Server.Data;
using Image_Server.Models;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;


namespace Image_Server.Controllers
{    
    [Route("api")]
    [ApiController]
    public class UploadFileController : ControllerBase
    {
        
        private readonly IFileUploadRepo _repository;
        
        public UploadFileController(IFileUploadRepo repository)
        {
            _repository = repository;
        }
        [Route("UploadFile")]
        [HttpPost,DisableRequestSizeLimit]
        public IActionResult UploadFile()
        {
            try
            {
                string jpgType = ".jpg";
                string pgnType = ".png";
                var file = Request.Form.Files[0];
                string tempfolderName = Path.Combine("StaticFiles", "Uploads","temp");
                string folderName = Path.Combine("StaticFiles", "Uploads");
                string jpgFolderName = Path.Combine("StaticFiles", "Uploads", "jpg");
                string pgnFolderName = Path.Combine("StaticFiles", "Uploads", "png");
                string original = "orginal";
                string size256x256 = "256x256";
                string size680x680 = "680x680";
                string size1920x1080  = "1920x1080";
                string tempPathToSave = Path.Combine(Directory.GetCurrentDirectory(), tempfolderName);
                if(file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim().ToString();
                 
                    var newFileName = Guid.NewGuid();
                    
                    string tempfullPath = Path.Combine(tempfolderName, fileName);
                    
                    string dbPath = Path.Combine(Directory.GetCurrentDirectory(), newFileName.ToString());
                    using (var stream = new FileStream(tempfullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    // check if the file was created, and if so get the file type and move it to the right folder.
                    if(System.IO.File.Exists(tempfullPath))
                    {
                        // get filetype
                        var fileType = Path.GetExtension(tempfullPath);
                        // if jpg, move it to the jpg folder, and if succes, delete the temp file.
                        if(fileType == jpgType)
                        {
                          string orgiPath = Path.Combine(jpgFolderName, newFileName + jpgType);
                            System.IO.File.Move(tempfullPath, Path.Combine(jpgFolderName, newFileName + jpgType));
                            if(System.IO.File.Exists(orgiPath))
                            {
                                
                                System.IO.File.Copy(orgiPath, Path.Combine(jpgFolderName, original, newFileName + jpgType), true);
                                var saveName = new FileUpload();
                                saveName.OldFileName = fileName;                          
                                saveName.NewFileName = Path.Combine(newFileName + jpgType);
                                _repository.SaveFileName(saveName);
                                _repository.SaveChanges();

                                // 256
                                string path256 = Path.Combine(jpgFolderName, size256x256, newFileName +"size256X256"+ jpgType);
                                System.IO.File.Copy(orgiPath, path256, true);
                                using var image256 = Image.Load(path256);
                                image256.Mutate(x => x.Resize(256, 256));
                                image256.Save(path256);

                                var saveName256 = new FileUpload();
                                saveName256.OldFileName = fileName;                          
                                saveName256.NewFileName = Path.Combine(newFileName +"size256X256"+ jpgType);
                                _repository.SaveFileName(saveName256);
                                _repository.SaveChanges();

                                // 680
                                string path680 = Path.Combine(jpgFolderName, size680x680, newFileName +"size680x680"+ jpgType);
                                System.IO.File.Copy(orgiPath, path680, true);
                                using var image680 = Image.Load(path680);
                                image680.Mutate(x => x.Resize(680, 680));
                                image680.Save(path680);

                                var saveName680 = new FileUpload();
                                saveName680.OldFileName = fileName;                          
                                saveName680.NewFileName = Path.Combine(newFileName +"size680x680"+ jpgType);
                                _repository.SaveFileName(saveName680);
                                _repository.SaveChanges();

                                // 1920
                                string path1920 = Path.Combine(jpgFolderName, size1920x1080, newFileName +"size1920x1080"+ jpgType);
                                System.IO.File.Copy(orgiPath, path1920, true);
                                using var image1920 = Image.Load(path1920);
                                image1920.Mutate(x => x.Resize(1920, 1080));
                                image1920.Save(path1920);
                                var saveName1920 = new FileUpload();
                                saveName1920.OldFileName = fileName;                          
                                saveName1920.NewFileName = Path.Combine(newFileName +"size1920x1080"+ jpgType);
                                _repository.SaveFileName(saveName1920);
                                _repository.SaveChanges();

                            }
                            System.IO.File.Delete(orgiPath);
                        }
                        // if pgn, move it to the jpg folder, and if succes, delete the temp file.
                        if(fileType == pgnType)
                        {
                            string orgiPath = Path.Combine(pgnFolderName, newFileName +  pgnType);
                            System.IO.File.Move(tempfullPath, Path.Combine(pgnFolderName, newFileName + pgnType));
                            
                            if(System.IO.File.Exists(orgiPath))
                            {
                                
                                System.IO.File.Copy(orgiPath, Path.Combine(pgnFolderName, original, newFileName + pgnType), true);
                                var saveName = new FileUpload();
                                saveName.OldFileName = fileName;                          
                                saveName.NewFileName = Path.Combine(newFileName + pgnType);
                                _repository.SaveFileName(saveName);
                                _repository.SaveChanges();

                                // 256
                                string path256 = Path.Combine(pgnFolderName, size256x256, newFileName +"size256X256"+ pgnType);
                                System.IO.File.Copy(orgiPath, path256, true);
                                using var image256 = Image.Load(path256);
                                image256.Mutate(x => x.Resize(256, 256));
                                image256.Save(path256);

                                var saveName256 = new FileUpload();
                                saveName256.OldFileName = fileName;                          
                                saveName256.NewFileName = Path.Combine(newFileName +"size256X256"+ pgnType);
                                _repository.SaveFileName(saveName256);
                                _repository.SaveChanges();

                                // 680
                                string path680 = Path.Combine(pgnFolderName, size680x680, newFileName +"size680x680"+ pgnType);
                                System.IO.File.Copy(orgiPath, path680, true);
                                using var image680 = Image.Load(path680);
                                image680.Mutate(x => x.Resize(680, 680));
                                image680.Save(path680);

                                var saveName680 = new FileUpload();
                                saveName680.OldFileName = fileName;                          
                                saveName680.NewFileName = Path.Combine(newFileName +"size680x680"+ pgnType);
                                _repository.SaveFileName(saveName680);
                                _repository.SaveChanges();

                                // 1920
                                string path1920 = Path.Combine(pgnFolderName, size1920x1080, newFileName +"size1920x1080"+ pgnType);
                                System.IO.File.Copy(orgiPath, path1920, true);
                                using var image1920 = Image.Load(path1920);
                                image1920.Mutate(x => x.Resize(1920, 1080));
                                image1920.Save(path1920);
                                var saveName1920 = new FileUpload();
                                saveName1920.OldFileName = fileName;                          
                                saveName1920.NewFileName = Path.Combine(newFileName +"size1920x1080"+ pgnType);
                                _repository.SaveFileName(saveName1920);
                                _repository.SaveChanges();
                                System.IO.File.Delete(orgiPath);
                            
                            }
                        
                        }
                    }
                    // return the path saved to front
                    return Ok(new { dbPath});
                  //  SaveFileName(FileUpload fileUpload)
                }
                else
                {
                    return BadRequest();
                }
            }
            catch(Exception ex)
            {
                return StatusCode(500, $"internal server error: {ex}");
            } 
        }

    }
}