using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using Image_Server.Data;
using Image_Server.Models;
using Image_Server.Dtos;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.Formats;

namespace Image_Server.Controllers
{
    [Route("api")]
    [ApiController]
    public class ImageResizeController : ControllerBase
    {
        
        private readonly IFileUploadRepo _repository;
        
        public ImageResizeController(IFileUploadRepo repository)
        {
            _repository = repository;
        }
        private FilePathAndTypeDto FindFiles(string getFileName)
        {
            var pathandtype = new FilePathAndTypeDto();
            // Get the upload basefolder. If this example no user is given, but here a user could be specificed a that users folder found. 
            string folderName = Path.Combine("StaticFiles", "Uploads");
            // Fetch the new filename from the database and populate it to the FileUpload model.
            
            // get the selected file's type and remove the dot.
            pathandtype.fileType = Path.GetExtension(getFileName).Replace(".", "");
            // search the directory (and sub folders - SearchOption.AllDirectories) specified in folderName.
            string[] filePath = Directory.GetFiles(folderName,getFileName, SearchOption.AllDirectories);
            // Since each filename is uniq, the first item in the filePath array will always be the selected file. 
            pathandtype.filePath = filePath[0];
            return pathandtype;
        }
        [HttpPost]
        [Route("size256X256")]
        public FileStreamResult size256(FileUploadJsonDto getFileName)
        {
            var newFileNameFolderpath = FindFiles(getFileName.NewFileName);
           var fileItem = _repository.GetFileByName(getFileName.NewFileName);
            // return the file to the user and set contenttype to image/ + fileType
            var imf = Image.DetectFormat(newFileNameFolderpath.filePath);
             using var image256 = Image.Load(newFileNameFolderpath.filePath);
            image256.Mutate(x => x.Resize(256, 256));
            var streamtosave = new FileStream(Path.Combine("StaticFiles", "Uploads", "temp" + "temp"+ "."+newFileNameFolderpath.fileType), System.IO.FileMode.Create);
            image256.Save(streamtosave, imf);
             // open a filesteam 
            var stream = new FileStream(newFileNameFolderpath.filePath, System.IO.FileMode.Open);
            System.IO.File.Delete(Path.Combine("StaticFiles", "Uploads", "temp" + "temp"+ "."+newFileNameFolderpath.fileType));
            return new FileStreamResult(stream, imf.DefaultMimeType)
            {
                // rename the file to the users original filename 
                FileDownloadName = fileItem.OldFileName 
            };
        
        }
        [HttpPost]
        [Route("size680x680")]
        public FileStreamResult size680 (FileUploadJsonDto getFileName)
        { 
            var fileItem = _repository.GetFileByName(getFileName.NewFileName);
            var newFileNameFolderpath = FindFiles(getFileName.NewFileName);
            // return the file to the user and set contenttype to image/ + fileType
            var imf = Image.DetectFormat(newFileNameFolderpath.filePath);
             using var image680 = Image.Load(newFileNameFolderpath.filePath);
            image680.Mutate(x => x.Resize(680, 680));
            var streamtosave = new FileStream(Path.Combine("StaticFiles", "Uploads", "temp" + "temp"+ "."+newFileNameFolderpath.fileType), System.IO.FileMode.Create);
            image680.Save(streamtosave, imf);
            // open a filesteam 
            var stream = new FileStream(newFileNameFolderpath.filePath, System.IO.FileMode.Open);
            
            return new FileStreamResult(stream, "image/"+ newFileNameFolderpath.fileType)
            {
                // rename the file to the users original filename 
                FileDownloadName = fileItem.OldFileName 
            };
        }

        [HttpPost]
        [Route("size1920x1080")]
        public FileStreamResult size1920 (FileUploadJsonDto getFileName)
        {
           
            // Fetch the new filename from the database and populate it to the FileUpload model.
            var fileItem = _repository.GetFileByName(getFileName.NewFileName);
             var newFileNameFolderpath = FindFiles(getFileName.NewFileName);
           
            // return the file to the user and set contenttype to image/ + fileType
            var imf = Image.DetectFormat(newFileNameFolderpath.filePath);
             using var image1920 = Image.Load(newFileNameFolderpath.filePath);
            image1920.Mutate(x => x.Resize(1920, 1080));
             var streamtosave = new FileStream(Path.Combine("StaticFiles", "Uploads", "temp" + "temp"+ "."+newFileNameFolderpath.fileType), System.IO.FileMode.Create);
            image1920.Save(streamtosave, imf);
             // open a filesteam 
            var stream = new FileStream(newFileNameFolderpath.filePath, System.IO.FileMode.Open);
            System.IO.File.Delete(Path.Combine("StaticFiles", "Uploads", "temp" + "temp"+ "."+newFileNameFolderpath.fileType));
            return new FileStreamResult(stream, "image/"+ newFileNameFolderpath.fileType)
            {
                // rename the file to the users original filename 
                FileDownloadName = fileItem.OldFileName 
            };
        }
        [HttpPost]
        [Route("dynamic")]
        public FileStreamResult dynamicResize (DynamicJsonDto dynamicFile)
        {
           
            // Fetch the new filename from the database and populate it to the FileUpload model.
            var fileItem = _repository.GetFileByName(dynamicFile.NewFileName);
             var newFileNameFolderpath = FindFiles(dynamicFile.NewFileName);
           
            // return the file to the user and set contenttype to image/ + fileType
            var imf = Image.DetectFormat(newFileNameFolderpath.filePath);
             using var image1920 = Image.Load(newFileNameFolderpath.filePath);
             // size format is X, X
            image1920.Mutate(x => x.Resize(dynamicFile.width, dynamicFile.height));
            // save a temp file 
            var streamtosave = new FileStream(Path.Combine("StaticFiles", "Uploads", "temp" + "temp"+ "."+newFileNameFolderpath.fileType), System.IO.FileMode.Create);
            image1920.Save(streamtosave, imf);
             // open a filesteam 
            var stream = new FileStream(newFileNameFolderpath.filePath, System.IO.FileMode.Open);
            // delete the temp file.
            System.IO.File.Delete(Path.Combine("StaticFiles", "Uploads", "temp" + "temp"+ "."+newFileNameFolderpath.fileType));
            return new FileStreamResult(stream, "image/"+ newFileNameFolderpath.fileType)
            {
                // rename the file to the users original filename 
                FileDownloadName = fileItem.OldFileName 
            };
        }
    }
}