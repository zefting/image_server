using Image_Server.Models;
namespace Image_Server.Data
{        
    public interface IFileUploadRepo
    {
        // Beers GetBeerById(int Beer_id);
        FileUpload GetFileByName(string getFileName);
        bool SaveChanges();
        void SaveFileName(FileUpload fileUpload);
    }
    
}