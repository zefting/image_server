using Image_Server.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Linq;
namespace Image_Server.Data
{
    public class FileUploadImpli : IFileUploadRepo
    {
        private readonly FileUpload2Context _context;
        public FileUploadImpli(FileUpload2Context context)
        {
            _context = context;
        }
        public bool SaveChanges()
        {
            return(_context.SaveChanges() >=0);
        }
        public void SaveFileName(FileUpload fileUpload)
        {
            if(fileUpload == null)
            {
                throw new ArgumentNullException(nameof(fileUpload));
            }
            _context.FileUpload.Add(fileUpload);
        }
        public FileUpload GetFileByName(string getFileName)
        {
            return _context.FileUpload.FirstOrDefault(p => p.NewFileName == getFileName);
        }
    }
}